from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from authenticator import authenticator
from routers import accounts, watchlist, anime

app = FastAPI()

app.include_router(authenticator.router, tags=["Authentication"])

app.include_router(accounts.router, tags=["Authentication"])

app.include_router(watchlist.router, tags=["Watchlist"])

app.include_router(anime.router, tags=["Anime"])


@app.get("/")
def home():
    return True


app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
