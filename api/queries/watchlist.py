from pydantic import BaseModel
from pymongo import MongoClient
from typing import List
from .anime import AnimeOut
import os


DATABASE_URL = os.environ.get("DATABASE_URL")
client = MongoClient(DATABASE_URL)
db = client["project-management-database"]
collection = db["watchlists"]


class DuplicateWatchlistItem(ValueError):
    pass


class WatchlistItemIn(BaseModel):
    anime_id: int


class WatchlistItemOut(BaseModel):
    user_email: str
    anime: AnimeOut


class Watchlist(BaseModel):
    watchlist: List[WatchlistItemOut]


class WatchlistQueries:
    def get_watchlist(self, email: str) -> Watchlist:
        result = []
        for doc in collection.find({"user_email": email}):
            result.append(doc)
        return Watchlist(watchlist=result)

    def create(
        self,
        info: AnimeOut,
        email: str,
    ) -> WatchlistItemOut:
        for anime in self.get_watchlist(email=email).watchlist:
            if info.anime_id == anime.anime.anime_id:
                raise DuplicateWatchlistItem("Anime already in watch list")
        collection.insert_one(
            {
                "user_email": email,
                "anime": {
                    "anime_id": info.anime_id,
                    "small_image": info.small_image,
                    "large_image": info.large_image,
                    "title": {
                        "english": info.title.english,
                        "japanese": info.title.japanese,
                    },
                    "type": info.type,
                    "episodes": info.episodes,
                    "status": info.status,
                    "duration": info.duration,
                    "rating": info.rating,
                    "score": info.score,
                    "scored_by": info.scored_by,
                    "rank": info.rank,
                    "synopsis": info.synopsis,
                    "season": info.season,
                    "year": info.year,
                    "studio": {
                        "studio_id": info.studio.studio_id,
                        "type": info.studio.type,
                        "name": info.studio.name,
                    },
                    "genre": [
                        {
                            "id": info.genre[0].id if info.genre else None,
                            "name": info.genre[0].name if info.genre else None,
                        }
                    ],
                },
            }
        )
        return WatchlistItemOut(anime=info, user_email=email)

    def delete(self, email: str, info: WatchlistItemIn) -> bool:
        if collection.find_one(
            {
                "anime.anime_id": info.anime_id,
                "user_email": email,
            }
        ):
            collection.delete_one(
                {
                    "anime.anime_id": info.anime_id,
                    "user_email": email,
                }
            )
            return True
        else:
            return False
