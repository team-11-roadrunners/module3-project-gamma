from pydantic import BaseModel
from pymongo import MongoClient
import os

DATABASE_URL = os.environ.get("DATABASE_URL")
client = MongoClient(DATABASE_URL)
db = client["project-management-database"]
collection = db["user_accounts"]
collection.create_index("email", unique=True)


class UserNotLoggedInError(TypeError):
    pass


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    email: str
    password: str
    full_name: str


class AccountOut(BaseModel):
    id: str
    email: str
    full_name: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountQueries:
    def get(self, email: str) -> AccountOutWithPassword:
        user_data = collection.find_one({"email": email})
        account_out = AccountOutWithPassword(
            id=str(user_data["_id"]),
            email=user_data["email"],
            full_name=user_data["full_name"],
            hashed_password=user_data["hashed_password"],
        )
        return account_out

    def create(
        self, info: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        if collection.find_one({"email": info.email}):
            raise DuplicateAccountError

        new_user_data = {
            "email": info.email,
            "full_name": info.full_name,
            "hashed_password": hashed_password,
        }
        id = collection.insert_one(new_user_data).inserted_id
        new_user = AccountOutWithPassword(
            id=str(id),
            email=info.email,
            full_name=info.full_name,
            hashed_password=hashed_password,
        )
        return new_user
