from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.watchlist import WatchlistQueries
from sample_query_data import watchlist, watchlistItemData
from queries.watchlist import WatchlistItemOut, WatchlistItemIn
from queries.anime import (
    AnimeQueries,
    AnimeList,
    AnimeOut,
    Genre,
    Titles,
    Studio,
)


client = TestClient(app)


class CustomTestClient(TestClient):
    def delete_with_payload(self, **kwargs):
        return self.request(method="DELETE", **kwargs)


def fake_get_current_account_data():
    return {"email": "string"}


class fakeAnimeQueries:
    def get_one_anime(self, anime_id):
        result = AnimeList(anime=[])
        result.anime.append(self.create_anime_out())
        return result

    def create_anime_out(self):
        result = AnimeOut(
            anime_id=0,
            title=Titles(
                english="english",
                japanese="japanese",
            ),
            studio=Studio(),
            genre=[Genre()],
        )
        return result


class fakeWatchlistQueries:
    def create(self, info: AnimeOut, email: str):
        return WatchlistItemOut(
            user_email="string",
            anime=info,
        )

    def delete(self, email: str, info: WatchlistItemIn):
        return True

    def get_watchlist(self, email: str):
        return {
            "watchlist": [
                {
                    "user_email": "string",
                    "anime": {
                        "anime_id": 0,
                        "small_image": "string",
                        "large_image": "string",
                        "title": {"english": "string", "japanese": "string"},
                        "type": "string",
                        "episodes": 0,
                        "status": "string",
                        "duration": "string",
                        "rating": "string",
                        "score": 0,
                        "scored_by": 0,
                        "rank": 0,
                        "synopsis": "string",
                        "season": "string",
                        "year": 0,
                        "studio": {
                            "studio_id": 0,
                            "type": "string",
                            "name": "string",
                        },
                        "genre": [{"id": 0, "name": "string"}],
                    },
                }
            ]
        }


def test_get_watchlist():
    app.dependency_overrides[WatchlistQueries] = fakeWatchlistQueries
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data
    res = client.get("/api/watchlist")
    res_data = res.json()
    assert res.status_code == 200
    assert res_data == watchlist


def test_add_to_watchlist():
    app.dependency_overrides[WatchlistQueries] = fakeWatchlistQueries
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[AnimeQueries] = fakeAnimeQueries
    watchlist_in = {"anime_id": 0}
    res = client.post("/api/watchlist", json=watchlist_in)
    res_data = res.json()
    assert res.status_code == 200
    assert res_data == watchlistItemData


def test_delete_watchlist_item():
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[WatchlistQueries] = fakeWatchlistQueries
    client = CustomTestClient(app)
    watchlist_in = {"anime_id": 0}
    res = client.delete_with_payload(url="/api/watchlist", json=watchlist_in)
    data = res.json()
    assert res.status_code == 200
    assert data is True
