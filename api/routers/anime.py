from fastapi import APIRouter, Depends
from queries.anime import AnimeList, AnimeQueries, GenreList, GenreQueries
from authenticator import authenticator


router = APIRouter()


@router.get("/api/anime", response_model=AnimeList)
def get_anime(
    search: str = None,
    page: int = None,
    type: str = None,
    min_score: int = None,
    max_score: int = None,
    status: str = None,
    rating: str = None,
    genres: str = None,
    genres_exclude: str = None,
    order_by: str = None,
    sort: str = None,
    start_date: str = None,
    end_date: str = None,
    queries: AnimeQueries = Depends(),
):
    params = {
        "q": search,
        "page": page,
        "type": type,
        "min_score": min_score,
        "max_score": max_score,
        "status": status,
        "rating": rating,
        "genres": genres,
        "genres_exclude": genres_exclude,
        "order_by": order_by,
        "sort": sort,
        "start_date": start_date,
        "end_date": end_date,
    }
    return queries.list_anime(params)


@router.get("/api/anime/get/{anime_id}", response_model=AnimeList)
def get_one_anime(
    anime_id: int,
    queries: AnimeQueries = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.get_one_anime(anime_id)


@router.get("/api/anime/random", response_model=AnimeList)
def get_random_anime(
    search: str = None,
    type: str = None,
    min_score: int = None,
    max_score: int = None,
    status: str = None,
    rating: str = None,
    genres: str = None,
    genres_exclude: str = None,
    start_date: str = None,
    end_date: str = None,
    queries: AnimeQueries = Depends(),
):
    params = {
        "q": search,
        "type": type,
        "min_score": min_score,
        "max_score": max_score,
        "status": status,
        "rating": rating,
        "genres": genres,
        "genres_exclude": genres_exclude,
        "start_date": start_date,
        "end_date": end_date,
    }
    return queries.get_random_anime(params)


@router.get("/api/anime/genres", response_model=GenreList)
def get_genres(queries: GenreQueries = Depends()):
    return queries.get_genres()
