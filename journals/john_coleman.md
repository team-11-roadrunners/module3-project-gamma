06/26/2023 (Driver: Ronan)
Working backend authentication. Figured out how to create collections and add users to mongodb. Ready to begin making endpoints.

06/27/2023 (Jeremiah)
Set up watchlist endpoints with authentication. Began anime list endpoints with external api.

06/28/2023 (Austin)
Discussed functionality implementations with team. Added endpoints for external api—one for getting anime data by id, and one for getting a random anime. Refactored router tags. Deleted print statement in queries/accounts.

06/29/2023 (Me)
Fixed bugs with endpoints. Added genre endpoint. Started work on frontend. Ran into issues with UseEffect loading images from the api multiple times.

06/30/2023 (Ronan)
Worked on main page and footer elements. Added random page, detail page, and login page. 422 error on login. Need to figure out frontend auth.

07/10/2023 (Jeremiah)
Tried implementing frontend auth with jwtdown-for-react, ran into issues. Decided to wait until we learn to do auth with redux. Tried implementing redux, encountered various problems. Added secret key and updated docker-compose.

07/11/2023 (Austin)
Redux implementation is up and working. Got login and logout working. Signup is creating difficulties.

07/12/2023 (Me)
Driver. Signup working. Fixed minor issues with backend endpoints. Added AnimeCard component. Added WatchlistButton component. Added anime details page. TODO: add watchlist page and anime filter component.

07/13/2023 (Ronan/me)
Observer/Driver.
Created watchlist page. Reconfigured backend to store anime data in watchlist collection, in order to get around api rate limit.

07/14/2023 (Me)
Driver.
Created FilterBox component to update filter parameters. Updated AnimeList to include new component. Created filterSlice to store search filter parameters. Reconfigured searchbar to use new filterSlice. Reconfigured getAnime query to use filterSlice state values. FilterBox component working.

07/17/2023 (Jeremiah)
Created input components for FilterBox: YearSlider, ScoreSlider, Checkbox, and Radio.

07/18/2023 (Austin/Ronan)
Created unit tests for anime endpoints. Added functionality to list page to load more anime. Random page uses redux query and accepts filter parameters.

07/19/2023 (Ronan/Jeremiah)
Split into pairs. Jeremiah and austin worked on creating a carousel component for genres. Ronan and I implemented a carousel component for the random anime page. We then started working css.

07/20/2023 (Jeremiah)
Worked genre carousel, fixed some bugs, encountered others. Created add_to_watchlist unit test, broke others.

07/24/2023 (Me)
Finished unit tests, adding a test for the delete watchlist item endpoint. Worked on styling and refactoring. Created new pages for privacy policy and terms and conditions.

07/25/2023 (Austin, Jeremiah)
Split into pairs. Austin and I worked styling for the anime list page, including the filterbox and animeCard components. Jeremiah and Ronan worked styling for the main page, navbar, and login/signup pages.

07/26/2023 (Me, Ronan)
Split into pairs. Austin and I worked styling for the AnimeCard and footer components. We then moved on to create the AboutPreview component for the main page. I also started styling for the about page, adding components for gitlab and linkedin icons. Ronan and Jeremiah worked on the random page and the animeDetailCard component. We fixed a small bug when adding an anime with no genre to a watchlist.

07/27/2023 (Everyone)
Split into pairs. Ronan and Jeremiah finished styling for the random page. Austin and I set up the styling for the about page and adjusted the gitlab icon component. Joined back up to finish the detail page and redo the styling for the filterbox component.

07/28/2023 (Ronan/Me)
Worked as a goup. Finished up styling on the filterbox. Cleaned up code and organized file structure. Added protected routes with redirect.
