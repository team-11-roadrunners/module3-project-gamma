## 06/26/2023

We worked on the added MONGODB to the dockerfil, and we worked on AUTH. We were able to get our mongodb and mongo express to connect to our app and we finished the auth and are able to create a new authenticated user, see its token, log him out, and log him in again. Each user was added to the database.

## 06/27/2023

We worked an finished our endpoints for our watchlist, connecting it to auth and to our mongo database. We also built our third party API and got our get all to work. I was able to code along with my teammates, by giving them instructions and participating in figuring out how to make get our watchlist and 3rd party API work. I used the lectures and videos and documentations to find the right code to add.

## 06/28/2023

We worked on adding the enpoints of anime in routers and in queries. We finished up the anime queries class and added the function to get a randomized anime. Personally I helped on the research for the endpoints, using the external API's documentation and to make sure to return the right data. I was able to help fix some issues we had and some internal errors.

## 06/29/2023

We worked on the end of the backend by updating the randomizer function and then create new endpoints for being able to filter through anime genres. Then we started on the frontend with React and added a MainPage.js, Nav.js and updating the App.js. We also have added bootstrap to the index.html. I was able to help code by giving directions and overall vision of what we needed for the website. Also tried to resolve a few issues we had along the way.

## 06/30/23

We worked on the frontend and added a few new pages, such as the LoginPage, Random page, and AnimeDetail page. We didn't finish all of them as we were struggling with auth. I was the one coding and helping with writing the correct codes and to implement some specific features.

## 07/10/2023

We worked on the about page, added the html to it and also continued working on auth until we got noticed that we need to use redux with for auth. We had to wait for the lecture on auth and instead focused on setting up redux and access our 3rd party API though it. I was helping the team in reseatching docs and lectures on redux.

## 07/11/2023

We worked on auth for the front end using redux. We managed to get our login page to work and the logout as well. We also got the search bar functionality to work and take data from the third party API. I helped with giving some instructions and I did some researches for the issues we were having.

## 07/12/2023

We worked on the watchlist, adding an anime to the watchlist database, and removing it from the watchlist. Also, we implemented how we have to be logged in to watchlist an anime. I was helping with coding, researching and debugging.

## 07/13/2023

We worked on the watchlist page, and we were able to complete that feature. The 3rd party API didn't let us have more than 3 anime in the watchlist so we stored our watchlist to MongoDB instead. I was coding today and helped with brainstorming in how to inplement our ideas.

## 07/14/2023

We worked on the anime list page, and especially the filter box, in order to filter out any anime. It took us some time to implement the filter and reset functionalities but everything works now. I was helping with research, instructions and coding.

## 07/17/2023

We worked on the filterbox again and added a few funcitonalities such as radio, checkout and slider types. I was helping coding and researching, and tried to individually find solutions to our problems.

## 07/18/2023

We worked on the random page, and implemented redux for all of it, which took us a while. We also took a good amount of time to write a few tests. I participated in research and coded for half the day.

## 07/19/2023

We worked on the random page, adding a slide feature and some styling to it. We also worked on the main page, added some styling and functionality, as well as on the genre sections to add interactive cards. I was coding today and also helped with research.

## 07/20/2023

We worked on the main page and the genre section, mostly styling it and going over the code. We also worked on writing a test but haven't finished yet. I was helping with research and giving css ideas.

## 07/24/2023

We worked on testing and finished all the testing for our endpoints. We also styled the footer, added terms and conditions and privacy policy pages and started to work on the about page. I helped get the data for the terms and conditions and privacy policy, as well as the styling of the pages.

## 07/25/2023

We split the work in 2 groups. Jeremiah and I worked on the main page, styling and adding some features. We aslo worked on and finsihed the login and signup pages. I participated in css research and coding for the login and signup page.

## 07/26/2023

We split again and I was with Jeremiah. We worked on the detail page, and almost got it done. We finished the AnimeCardDetail too. I was coding today and had to learn more about css and how to implement an interactive button.

## 07/27/2023

We all did some coding today and worked on updating a few pages like the random page and also finished the anime detail page. We added styling to most pages and updating the filter side bar of the anime list page.

## 07/28/2023

Last day of the project. We mostly worked on the filter side bar and made some tweaking all over the website. Project is now done and ready for grading.
