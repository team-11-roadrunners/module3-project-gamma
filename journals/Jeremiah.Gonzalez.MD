## Journals

6/26 :
Worked with team to
    connect MongoDB and MongoExpress.
    create backend for queries, routers, and authenticator files to be able to create users.

6/27 :
Worked with the whole team to 
    Create enpoints for watchlist and to get anime list from external API.

6/28 :
Worked with the team to 
    Created enpoint to external API to get a specific anime data, with only tailored data returned. 
    Created endpoint to get data from external API for randomizer. 
    
6/29:
Worked with the team to
    complete the rest of the backend code, by debugging endpoints to randomizer, get genre endpoint, and params for all searches.
    Start with react on mainpage and navbar.

6/30: 
Worked with team
    to start creating frontend skeleton for the webpages. Started by creating main page, footer, details page, random page, and login page components.

7/10:
Worked with the team to
    Implement redux to project. 
    Started proccess to access api using redux. 
    
7/11:
Worked with the team to 
    Implement redux for auth functionality within frontend; login, logout.
    Implemented a filtered search feature within the nav bar and for future use in AnimeList page.

7/12:
worked with the team to
    Finish auth functionality; signup.
    Utilized redux to create watchlistbutton.jsx, AnimeCard.jsx, and AnimeList.js.
    Fixed backend bugs, such as: fastAPI response models in Add to Watchlist to match the data given from the external API.

7/13:
Worked with the team to
    work around api fetch limitations by storing anime data in mongo database. 
    created and finished watchlist component.

7/14:
Worked with team to 
    finish anime list page component. Started on the filterbox component to be able to creat search filters on anime list page.

7/17:
Worked with team as coder to
    implement functionality in filterbox component. Created YearSlider, Radio, Score, and Checkbox components in order to do so.
    
7/18:
Worked with the team to:
    created unit tests for anime endpoints.
    created see more button functionality in anime list page.
    created search filtering and implemented redux into randomize page.

7/19:
Worked with the team to:
    create unit test for one of the watchlist endpoints.
    Started implementing styling via css and bootstrap.
    Broke away from group with another team member, worked as head coder in our team, to work on carousel for genres on main page while the other half of the group worked on carousel for randomize page. 

7/20:
Worked with team as main coder to:
	worked on and fixed bugs on carousel, and created add_to_watchlist unit test. 

7/24:
Worked with team to 
	Create delete unit test and fixed anime unit tests to work. 
	
	
7/25:
Split up into two teams.
	Worked with half of the team as lead coder to implement css styling to 		navbar, main page, login, and signup pages. 
	Fixed error handling bugs for signup page. 
	Other half of the team worked on styling for the anime list page, and 			filterbox component. 

7/26:
Split up into two teams
	Worked with half of the team to work on styling for the randomize page and fixed backend to accept genre data correctly.
	Second half of the team worked on styling for about page, added “Meet the team” on main page, and finished anime list page.

7/27:
Split up into two teams and worked all together
	Worked with half the team as shared coder to complete anime random 		page.
	Worked all together as a group to finish details page, about page, and do 	updates to the filterbox component.

7/28
Worked with team to 
	Created styling for sidebar in filterbox component.
	Cleared console.logs all throughout projects code. 
	Added redirect login features for users that arent logged in.
	Formatted code with flake 8