import { createSlice } from "@reduxjs/toolkit";

export const filterSlice = createSlice({
    name: 'filter',
    initialState: {
        value: {
            search:null,
            page: null,
            type: null,
            min_score: null,
            max_score: null,
            status: null,
            rating: null,
            genres: null,
            genres_exclude: null,
            order_by: "members",
            sort: "desc",
            start_date: null,
            end_date: null,
        },
    },
    reducers: {
        filterParams: (state, action)=>{
            state.value = {
                'search': state.value.search,
                'page': state.value.page,
                ...action.payload,
            }
        },
        updateSearch: (state, action) => {
            state.value.search = action.payload
        },
        reset: (state, action) => {
            state.value = filterSlice.getInitialState().value
        },
        updateGenre: (state, action) => {
            state.value.genres = action.payload
        },
    },
})

export const {updateGenre, filterParams, updateSearch, reset} = filterSlice.actions
export default filterSlice.reducer
