import { Link } from "react-router-dom";

export default function Footer() {
  return (
    <footer className="footer py-3">
      <ul className="nav justify-content-center pb-3 mb-3">
        <li className="nav-item">
          <Link to="/anime/list" className="link px-2 footer-link">
            Anime
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/about" className="link px-2 footer-link">
            About
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/privacy" className="link px-2 footer-link">
            Privacy Policy
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/terms" className="link px-2 footer-link">
            Terms and Conditions
          </Link>
        </li>
      </ul>
      <p className="text-center">© 2023 RANDIME, all rights reserved</p>
    </footer>
  );
}
