import { updateSearch } from "../app/filterSlice";
import { useState } from "react";
import { useDispatch } from "react-redux";

export const SearchBar = () => {
  const dispatch = useDispatch();
  const [searchCriteria, setSearchCriteria] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(updateSearch(searchCriteria));
  };

  return (
    <form className="form-inline my-2 my-lg-0 overlay" onSubmit={handleSubmit}>
      <div className="input-group">
        <input
          type="text"
          className="form-control mr-sm-2 searchbar"
          value={searchCriteria}
          onChange={(e) => setSearchCriteria(e.target.value)}
        />
        <button className="btn btn-outline search-button" type="submit">
          Search
        </button>
      </div>
    </form>
  );
};
