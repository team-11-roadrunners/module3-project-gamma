import { useGetAccountQuery } from "../app/apiSlice";
import { Link } from "react-router-dom";
import WatchlistButton from "../Watchlist/WatchListButton";

export default function AnimeCardDetail({ props, anime }) {
  const { data: account } = useGetAccountQuery();

  const maxSynopsisLength = 300;
  const maxTitleLength = 39;

  const truncateSynopsis = (text) => {
    if (text?.length > maxSynopsisLength) {
      return text.substring(0, maxSynopsisLength) + "...";
    }
    return text;
  };

  const truncateTitle = (text) => {
    if (text?.length > maxTitleLength) {
      return text.substring(0, maxTitleLength) + "...";
    }
    return text;
  };

  return (
    <div className="card mb-3 random-cards" {...props}>
      <div>
        <div className="random-image">
          <img src={anime?.large_image} className="rounded-start" alt="" />
          <div className="position-absolute top-0 start-0">
            <p className="random-score card-text">{anime?.score}</p>
          </div>
          <div>
            {account && (
              <WatchlistButton
                className="random-watchlist"
                removeClass="remove-watchlist"
                anime_id={anime?.anime_id}
              />
            )}
          </div>
        </div>
      </div>
      <div>
        <div className="card-body">
          <h4 className="random-card-title">
            {truncateTitle(anime?.title?.english)}
          </h4>
          <p className="card-text">
            <strong>Genre: </strong>
            {anime?.genre.map((genre) => `${genre.name}`).join(", ")}
          </p>
          <p className="card-text">
            <strong>Type: </strong>
            {anime?.type}
          </p>
          <p className="card-text">
            <strong>Episode: </strong>
            {anime?.episodes}
          </p>
          <p className="card-text synopsis">
            <strong>Synopsis: </strong>
            {truncateSynopsis(anime?.synopsis)}
          </p>
          {account && (
            <Link className="random-details" to={`/anime/${anime?.anime_id}`}>
              More details...
            </Link>
          )}
        </div>
      </div>
    </div>
  );
}
