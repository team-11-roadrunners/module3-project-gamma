import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useGetRandimeQuery } from "../app/apiSlice";
import { Carousel } from "3d-react-carousal";
import AnimeCardDetail from "../Anime/AnimeCardDetail";
import RandomizeComponent from "../RandomizeButton/RandomizeButton";

export default function Random() {
  const searchCriteria = useSelector((state) => state.filter.value);
  const [previousAnimes, setPreviousAnimes] = useState([]);
  const [nextAnimes, setNextAnimes] = useState([]);
  const [currentAnime, setCurrentAnime] = useState(null);
  const { data, isLoading, refetch } = useGetRandimeQuery(searchCriteria);
  const [allAnime, setAllAnime] = useState([]);

  function fetchAnime() {
    refetch();
  }
  useEffect(() => {
    if (!isLoading && data?.anime) {
      setCurrentAnime(data.anime[0]);
    }
  }, [isLoading, data]);

  useEffect(() => {
    setAllAnime([...previousAnimes, currentAnime, ...nextAnimes]);
  }, [nextAnimes, currentAnime, previousAnimes]);

  function handleNext() {
    if (currentAnime) {
      setPreviousAnimes((prevStack) => [currentAnime, ...prevStack]);
      setCurrentAnime(nextAnimes[0]);
      setNextAnimes((prevStack) => prevStack.slice(1));
    }
    if (nextAnimes.length < 1) {
      fetchAnime();
    }
  }

  if (isLoading || !currentAnime) return <div>Loading...</div>;

  return (
    <div>
      <div className="title-text">
        <h2>Randomize until you find something you like!</h2>
      </div>
      <div className="carousel-div">
        <Carousel
          slides={allAnime.map((anime) => (
            <AnimeCardDetail key={anime} anime={anime} />
          ))}
        />
      </div>
      <div className="randomize-component" onClick={handleNext}>
        <RandomizeComponent />
      </div>
    </div>
  );
}
