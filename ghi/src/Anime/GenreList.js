import { useDispatch } from "react-redux"
import AnimeList from "./AnimeList"
import { useParams } from "react-router-dom"
import { updateGenre } from "../app/filterSlice";


export default function GenreList() {
  const { genre: genreTitle } = useParams();
  const dispatch = useDispatch();

  const genres = [
    {id: 1, name: 'Action'},
    {id: 2, name: 'Adventure'},
    {id: 5, name: 'Avant Garde'},
    {id: 46, name: 'Award Winning'},
    {id: 28, name: 'Boys Love'},
    {id: 4, name: 'Comedy'},
    {id: 8, name: 'Drama'},
    {id: 10, name: 'Fantasy'},
    {id: 26, name: 'Girls Love'},
    {id: 47, name: 'Gourmet'},
    {id: 14, name: 'Horror'},
    {id: 7, name: 'Mystery'},
    {id: 22, name: 'Romance'},
    {id: 24, name: 'Sci-Fi'},
    {id: 36, name: 'Slice of Life'},
    {id: 30, name: 'Sports'},
    {id: 37, name: 'Supernatural'},
    {id: 41, name: 'Suspense'},
  ]

  const genreId = (genre) => {
    for (let obj of genres) {
        if (obj.name.toLowerCase() === genre.toLowerCase()) {
          return obj.id
        }
      }
    }
    dispatch(updateGenre(genreId(genreTitle)));

    return (
        <AnimeList />
    )
}