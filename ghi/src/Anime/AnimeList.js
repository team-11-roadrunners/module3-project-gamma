import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useGetAnimeQuery } from "../app/apiSlice";
import AnimeCard from "../Anime/AnimeCard";
import { FilterBox } from "../FilterBox/FilterBox";

const AnimeList = () => {
  const searchCriteria = useSelector((state) => state.filter.value);
  const { data, isLoading } = useGetAnimeQuery(searchCriteria);
  const [anime, setAnime] = useState([]);
  const [page, setPage] = useState(2);

  const { data: extraAnime } = useGetAnimeQuery({
    ...searchCriteria,
    page: page,
  });

  function ThreeIFY() {
    const remainder = anime.length % 3;
    if (extraAnime?.anime && extraAnime.anime.length < 3){return anime.length}
    return anime.length - remainder;
  }

  useEffect(() => {
    if (!isLoading && data && data.anime) {
      setAnime(data.anime);
    }
  }, [isLoading, data]);
  const handleLoadMoreAnime = () => {
    setAnime((prevAnime) => [...prevAnime, ...extraAnime.anime]);
    setPage(page + 1);
  };

  if (isLoading) return <div>Loading...</div>;
  return (
    <div className="mt-3 anime-list">
      <FilterBox />
      <div className="mt-3 column-2">
        <h1>
          Anime List{" "}
          <small className="text-body-secondary">{searchCriteria.search}</small>
        </h1>
        <div className="card-list">
          {anime.slice(0, ThreeIFY()).map((p) => (
            <AnimeCard key={p.anime_id} anime={p} />
          ))}
        </div>
        {extraAnime?.anime.length > 0 &&
          <button className="see-more-btn" onClick={handleLoadMoreAnime}>
            See More
          </button>
        }
      </div>
    </div>
  );
};

export default AnimeList;
