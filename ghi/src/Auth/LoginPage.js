import React, { useState, useEffect } from "react";
import { useGetAccountQuery, useLoginMutation } from "../app/apiSlice";
import { useNavigate, useParams } from "react-router-dom";

export default function LoginPage() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [login] = useLoginMutation();
  const { data: account } = useGetAccountQuery();
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = useState("");
  const { path } = useParams();

  useEffect(() => {
    if (account) {
      navigate(`/${path ? path.split(',').join('/'): ''}`);
    }
  }, [account, navigate, path]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await login({ username, password });

      if (response.data && response.data.access_token) {
      } else {
        setErrorMessage("Invalid username or password. Please try again.");
      }
    } catch (error) {
      console.error("Login failed:", error);
      setErrorMessage(
        "An error occurred during login. Please try again later."
      );
    }
  };

  return (
    <div className="card text-bg-light form-all">
      <h5 className="card-header form-top">Login</h5>
      <div className="card-body form-box">
        {errorMessage && (
          <div className="alert alert-danger" role="alert">
            {errorMessage}
          </div>
        )}
        <form onSubmit={(e) => handleSubmit(e)}>
          <div className="mb-3">
            <label className="form-label">Username:</label>
            <input
              name="username"
              type="text"
              className="form-control"
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Password:</label>
            <input
              name="password"
              type="password"
              className="form-control"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <input
              className="btn btn-primary submit-button"
              type="submit"
              value="Login"
            />
          </div>
        </form>
      </div>
    </div>
  );
}
