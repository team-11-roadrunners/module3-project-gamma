import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useGetAccountQuery, useSignupMutation } from "../app/apiSlice";

export default function SignUp() {
  const [email, setEmail] = useState("");
  const [fullName, setFullName] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");
  const [signup] = useSignupMutation();
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = useState("");
  const { data: account } = useGetAccountQuery();

  const handleSubmit = async (e) => {
    e.preventDefault();

    const body = {
      email: email,
      password: password,
      full_name: fullName,
    };

    if (password !== passwordConfirmation) {
      setErrorMessage("Password and Confirm Password do not match.");
      return;
    }
    const response = await signup(body);
    if (response.error) {
      console.error("Sign up failed");
      setErrorMessage("Username already exists. Please try again");
    }
  };

  useEffect(() => {
    if (account) {
      navigate("/");
    }
  }, [account, navigate]);

  return (
    <div className="card text-bg-light form-all">
      <h5 className="card-header form-top">Sign Up</h5>
      <div className="card-body form-box">
        {errorMessage && (
          <div className="alert alert-danger" role="alert">
            {errorMessage}
          </div>
        )}
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label className="form-label">Username:</label>
            <input
              value={email}
              name="email"
              type="text"
              className="form-control"
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Password:</label>
            <input
              value={password}
              name="password"
              type="password"
              className="form-control"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Confirm Password:</label>
            <input
              value={passwordConfirmation}
              name="passwordConfirmation"
              type="password"
              className="form-control"
              onChange={(e) => setPasswordConfirmation(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Full Name:</label>
            <input
              value={fullName}
              name="fullName"
              type="text"
              className="form-control"
              onChange={(e) => setFullName(e.target.value)}
            />
          </div>
          <div>
            <input
              className="btn btn-primary submit-button"
              type="submit"
              value="Signup!"
            />
          </div>
        </form>
      </div>
    </div>
  );
}
