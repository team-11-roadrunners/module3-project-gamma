import React, { useReducer } from "react";
import "./genre.css";
import Slide from "./Slide";
import { slides } from "./slideData";

const initialState = {
  slideIndex: 0,
};

const slidesReducer = (state, event) => {

  if (event.type === "NEXT") {
    return {
      ...state,
      slideIndex: (state.slideIndex + 1) % slides.length,

    };
  }
  if (event.type === "PREV") {
    return {
      ...state,
      slideIndex:
        state.slideIndex === 0 ? slides.length - 1 : state.slideIndex - 1,
    };
  }
};

export default function GenreSlider() {
  const [state, dispatch] = useReducer(slidesReducer, initialState);

  // list of slides
  // range -2, 2
  // if one side is empty, fill
  // change offset of left two most to be positive
  // (0, -1, -2, -3, -4)
  // (-2, -1, 0, 1, 2)


  return (
    <div className="slides">
      {[...slides].map((slide, i) => {
        return (
          <Slide
            slide={slide}
            key={i}
            offset={(state.slideIndex + i + 2) % slides.length - 2}
          />
        );
      })}
      <div className="button-div">
        <button
          className="buttons left"
          onClick={() => {
            dispatch({ type: "NEXT" });
          }}
        >
          «
        </button>
        <button
          className="buttons right"
          onClick={() => {
            dispatch({ type: "PREV" });
          }}
        >
          »
        </button>
      </div>
    </div>
  );
}
