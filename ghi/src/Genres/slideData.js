export const slides = [
  {
    title: "Action",
    subtitle: "sub",
    description: "dec",
    image: "https://images7.alphacoders.com/909/909480.jpg",
  },

  {
    title: "Adventure",
    subtitle: "sub",
    description: "desc",
    image:
      "https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F6%2F2022%2F12%2F16%2FPokemon-121622-1.jpg",
  },

  {
    title: "Comedy",
    subtitle: "sub",
    description: "desc",
    image:
      "https://www.themarysue.com/wp-content/uploads/2023/04/househusband.jpeg",
  },

  {
    title: "Romance",
    subtitle: "sub",
    description: "desc",
    image:
      "https://e0.pxfuel.com/wallpapers/448/166/desktop-wallpaper-marin-and-wakana-from-my-dress-up-darling-anime-ultra-my-dressup-darling.jpg",
  },

  {
    title: "Drama",
    subtitle: "sub",
    description: "esc",
    image:
      "https://res.cloudinary.com/jerrick/image/upload/d_642250b563292b35f27461a7.png,f_jpg,fl_progressive,q_auto,w_1024/wkjixtzjeq4btfd3vjed.jpg",
  },

  {
    title: "Horror",
    subtitle: "sub",
    description: "desc",
    image:
      "https://assets-prd.ignimgs.com/2022/09/27/tokyo-ghoul-1664245107129.jpeg",
  },
];
