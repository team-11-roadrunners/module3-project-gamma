import { useRef, useEffect } from "react";

export default function useTilt(active) {
  const ref = useRef(null);

  useEffect(() => {
    if (!ref.current || !active) {
      return;
    }

    const state = {
      rect: ref.current.getBoundingClientRect(),
      mouseX: undefined,
      mouseY: undefined,
    };

    const resizeObserver = new ResizeObserver((entries) => {
      if (ref.current) {
        state.rect = ref.current.getBoundingClientRect();
      }
    });

    const handleMouseMove = (e) => {
      if (!ref.current) {
        return;
      }
      // if (!state.rect) {
      //   state.rect = ref.current.getBoundingClientRect();
      // }
      state.mouseX = e.clientX;
      state.mouseY = e.clientY;
      const px = (state.mouseX - state.rect.left) / state.rect.width;
      const py = (state.mouseY - state.rect.top) / state.rect.height;
      if (ref.current) {
        ref.current.style.setProperty("--px", px);
        ref.current.style.setProperty("--py", py);
      }
    };

    ref.current.addEventListener("mousemove", handleMouseMove);

    resizeObserver.observe(ref.current);

    return () => {
      if (ref.current) {
        resizeObserver.unobserve(ref.current);
        // eslint-disable-next-line react-hooks/exhaustive-deps
        ref.current.removeEventListener("mousemove", handleMouseMove);
      }
    };
  }, [active]);

  return ref;
}
