import { useEffect, useState } from "react";
import {
  useAddWatchlistItemMutation,
  useDeleteWatchlistItemMutation,
  useGetWatchlistQuery,
} from "../app/apiSlice";

const WatchlistButton = ({ anime_id, className, removeClass }) => {
  const [addWatchlistItem] = useAddWatchlistItemMutation();
  const [deleteWatchlistItem] = useDeleteWatchlistItemMutation();
  const { data: watchlist } = useGetWatchlistQuery();
  const [watchlistItem, setWatchlistItem] = useState();

  useEffect(() => {
    if (watchlist) {
      setWatchlistItem(watchlist.find((f) => f.anime.anime_id === anime_id));
    }
  }, [anime_id, watchlist]);

  if (!watchlistItem)
    return (
      <button
        className={className + " no-box-shadow"}
        onClick={() => addWatchlistItem({ anime_id })}
      >
        <strong>+</strong>
      </button>
    );
  return (
    <button
      className={`btn btn-danger watchlist-button-remove no-box-shadow ${removeClass}`}
      onClick={() => deleteWatchlistItem({ anime_id })}
    >
      Remove from Watchlist
    </button>
  );
};

export default WatchlistButton;
