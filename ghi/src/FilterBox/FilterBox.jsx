import { filterParams, reset } from "../app/filterSlice";
import { useState } from "react";
import { useDispatch} from "react-redux";
import Radio from "./Radio";
import Checkbox from "./Checkbox";
import { useGetGenresQuery } from "../app/apiSlice";
import Score from "./Score";
import YearSlider from "./YearSlider";

export const FilterBox = () => {
  const dispatch = useDispatch();
  const [filterCriteria, setFilterCriteria] = useState({})
  const { data: genre, isLoading } = useGetGenresQuery();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFilterCriteria((prevFormData) => ({ ...prevFormData, [name]: value }));
  };

  const ignoreValue = (name, value) =>{
    if(filterCriteria[name]===value){
      delete filterCriteria[name]
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    ignoreValue('start_date','1960-01-01')
    ignoreValue('end_date', '2023-01-01')
    dispatch(filterParams(filterCriteria));
  };

  if (isLoading) return <div>Loading...</div>;

  return (
    <form className="filter-box" onSubmit={handleSubmit}>
      <div id="accordion-panel" className="accordion">
        <div className="accordion-item">
          <h2 className="accordion-header" id="panelsStayOpen-headingOne">
            <button
              className="accordion-button"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#panelsStayOpen-collapseOne"
              aria-expanded="false"
              aria-controls="panelsStayOpen-collapseOne"
            >
              Select a type
            </button>
          </h2>
          <div
            id="panelsStayOpen-collapseOne"
            className="accordion-collapse collapse"
            aria-labelledby="panelsStayOpen-headingOne"
          >
            <div className="accordion-body">
              <div className="form-floating mb-3">
                <Radio
                  func={handleChange}
                  name="type"
                  setFilterCriteria={setFilterCriteria}
                  options={["movie", "tv", "ova", "ona", "special", "music"]}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="panelsStayOpen-headingTwo">
            <button
              className="accordion-button collapsed"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#panelsStayOpen-collapseTwo"
              aria-expanded="false"
              aria-controls="panelsStayOpen-collapseTwo"
            >
              Select scores
            </button>
          </h2>
          <div
            id="panelsStayOpen-collapseTwo"
            className="accordion-collapse collapse"
            aria-labelledby="panelsStayOpen-headingTwo"
          >
            <div className="accordion-body">
              <div className="form-floating mb-3">
                <Score name="min_score" func={handleChange} value={7} />
              </div>
              <div className="form-floating mb-3">
                <Score name="max_score" func={handleChange} value={10} />
              </div>
            </div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="panelsStayOpen-headingThree">
            <button
              className="accordion-button collapsed"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#panelsStayOpen-collapseThree"
              aria-expanded="false"
              aria-controls="panelsStayOpen-collapseThree"
            >
              Select a status
            </button>
          </h2>
          <div
            id="panelsStayOpen-collapseThree"
            className="accordion-collapse collapse"
            aria-labelledby="panelsStayOpen-headingThree"
          >
            <div className="accordion-body">
              <div className="form-floating mb-3">
                <Radio
                  func={handleChange}
                  name="status"
                  setFilterCriteria={setFilterCriteria}
                  options={["airing", "complete", "upcoming"]}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="panelsStayOpen-headingFour">
            <button
              className="accordion-button collapsed"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#panelsStayOpen-collapseFour"
              aria-expanded="false"
              aria-controls="panelsStayOpen-collapseFour"
            >
              Select a rating
            </button>
          </h2>
          <div
            id="panelsStayOpen-collapseFour"
            className="accordion-collapse collapse"
            aria-labelledby="panelsStayOpen-headingFour"
          >
            <div className="accordion-body">
              <div className="form-floating mb-3">
                <Radio
                  func={handleChange}
                  name="rating"
                  setFilterCriteria={setFilterCriteria}
                  options={["g", "pg", "pg13", "r17", "r"]}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="panelsStayOpen-headingFive">
            <button
              className="accordion-button collapsed"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#panelsStayOpen-collapseFive"
              aria-expanded="false"
              aria-controls="panelsStayOpen-collapseFive"
            >
              Select a genre
            </button>
          </h2>
          <div
            id="panelsStayOpen-collapseFive"
            className="accordion-collapse collapse"
            aria-labelledby="panelsStayOpen-headingFive"
          >
            <div className="accordion-body">
              <div className="form-floating mb-3">
                <Checkbox
                  name="genres"
                  onChange={handleChange}
                  options={genre}
                  setFilterCriteria={setFilterCriteria}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="panelsStayOpen-headingSix">
            <button
              className="accordion-button collapsed"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#panelsStayOpen-collapseSix"
              aria-expanded="false"
              aria-controls="panelsStayOpen-collapseSix"
            >
              Select a genre to exculde
            </button>
          </h2>
          <div
            id="panelsStayOpen-collapseSix"
            className="accordion-collapse collapse"
            aria-labelledby="panelsStayOpen-headingSix"
          >
            <div className="accordion-body">
              <div className="form-floating mb-3">
                <Checkbox
                  name="genres_exclude"
                  options={genre}
                  onChange={handleChange}
                  setFilterCriteria={setFilterCriteria}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="panelsStayOpen-headingEight">
            <button
              className="accordion-button collapsed"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#panelsStayOpen-collapseEight"
              aria-expanded="false"
              aria-controls="panelsStayOpen-collapseEight"
            >
              Select a sort
            </button>
          </h2>
          <div
            id="panelsStayOpen-collapseEight"
            className="accordion-collapse collapse"
            aria-labelledby="panelsStayOpen-headingEight"
          >
            <div className="accordion-body">
              <div className="form-floating mb-3">
                <Radio
                  func={handleChange}
                  name="sort"
                  setFilterCriteria={setFilterCriteria}
                  options={["asc", "desc"]}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="panelsStayOpen-headingNine">
            <button
              className="accordion-button collapsed"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#panelsStayOpen-collapseNine"
              aria-expanded="false"
              aria-controls="panelsStayOpen-collapseNine"
            >
              Select a year
            </button>
          </h2>
          <div
            id="panelsStayOpen-collapseNine"
            className="accordion-collapse collapse"
            aria-labelledby="panelsStayOpen-headingNine"
          >
            <div className="accordion-body">
              <div className="form-floating mb-3">
                <YearSlider
                  name="start_date"
                  setFilterCriteria={setFilterCriteria}
                  value={1960}
                />
              </div>
              <div className="form-floating mb-3">
                <YearSlider
                  name="end_date"
                  setFilterCriteria={setFilterCriteria}
                  value={2023}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="accordion-item">
          <h2 className="accordion-header" id="panelsStayOpen-headingSeven">
            <button
              className="accordion-button collapsed"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#panelsStayOpen-collapseSeven"
              aria-expanded="false"
              aria-controls="panelsStayOpen-collapseSeven"
            >
              Order by
            </button>
          </h2>
          <div
            id="panelsStayOpen-collapseSeven"
            className="accordion-collapse collapse"
            aria-labelledby="panelsStayOpen-headingSeven"
          >
            <div className="accordion-body">
              <div className="form-floating mb-3">
                <Radio
                  func={handleChange}
                  name="order_by"
                  setFilterCriteria={setFilterCriteria}
                  options={[
                    "mal_id",
                    "title",
                    "type",
                    "rating",
                    "start_date",
                    "end_date",
                    "episodes",
                    "score",
                    "scored_by",
                    "rank",
                    "popularity",
                    "members",
                    "favorites",
                  ]}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="filter-button-div">
        <button className="filter-button" type="submit">
          Filter
        </button>
        <button
          className="filter-button"
          type="button"
          onClick={(e) => {
            dispatch(reset());
          }}
        >
          Reset
        </button>
      </div>
    </form>
  );
};
