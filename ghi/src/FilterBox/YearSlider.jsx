import { useEffect, useState } from "react";

export default function YearSlider({ name, setFilterCriteria, value = 2000 }) {
  const [scoreValue, setScoreValue] = useState(value);

  const handleChange = (e) => {
    const { value } = e.target;
    setScoreValue(value);
  };

  useEffect(() => {
    setFilterCriteria((prevFormData) => ({
      ...prevFormData,
      [name]: `${scoreValue}-01-01`,
    }));
  }, [name, scoreValue, setFilterCriteria]);
  return (
    <div>
      <p>
        {name.split("_").join(" ")}: {scoreValue}
      </p>
      <input
        type="range"
        name={name}
        min="1960"
        max="2023"
        value={scoreValue}
        onChange={handleChange}
      />
      <label htmlFor={name}>{name}</label>
    </div>
  );
}
