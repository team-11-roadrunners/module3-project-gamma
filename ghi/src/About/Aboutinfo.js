export const AboutInfo = [
    {
        name: "Austin",
        imageURL:
        "https://media.licdn.com/dms/image/C4D03AQFOK6sw_ai9RA/profile-displayphoto-shrink_200_200/0/1591642677197?e=1695859200&v=beta&t=1sLTQJ5kuRf8VIhQAjxAxABttVhpJ9DQMWNHmBZEFs4",
        linkedIn: "https://www.linkedin.com/in/9n2v7f/",
        gitlab: "https://gitlab.com/giants38914",
        bio: "A lover of plants & programming, I've been working remotely for nearly a decade now. With over three years of experience coding, I've built full-stack projects using php, Javascript & Python. But my learning journey isn't over & I'm now working towards obtaining a Bachelors in Computer Science at Palm Beach State College.",
    },
    {
        name: "Jeremiah",
        imageURL:
        "https://media.licdn.com/dms/image/D5603AQFuY3uBFv8i2w/profile-displayphoto-shrink_200_200/0/1687461036374?e=1695859200&v=beta&t=peR39ry2kb1kbdF4OD9BQxunwUYVqe3jIplrLTomf1g",
        linkedIn: "https://www.linkedin.com/in/jeremiah-gonzalez/",
        gitlab: "https://gitlab.com/jeremiah5799",
        bio: "🚀 Aspiring Full-Stack Software Engineer, Passionate about Learning & Growth  🚀. I firmly believe that the field of software development is constantly evolving, and I thrive on embracing new challenges and learning from each opportunity. My insatiable curiosity drives me to explore emerging technologies and innovative solutions, allowing me to stay ahead in this dynamic industry.",
    },
    {
        name: "Ronan",
        imageURL:
        "https://media.licdn.com/dms/image/C4E03AQHyix7EbYk7ZA/profile-displayphoto-shrink_200_200/0/1645506803872?e=1695859200&v=beta&t=pGVR_TjJotD7qFqWsJwCfkts9dgjwFdUnfsyGH0xdiI",
        linkedIn: "https://www.linkedin.com/in/ronan-fitamant/",
        gitlab: "https://gitlab.com/ronan.fitamant",
        bio: "I am a skilled Full Stack Software Engineer with a strong background in Python, Django, React, and JavaScript. My passion lies in developing comprehensive web applications and utilizing modern technologies to deliver seamless user experiences.",
    },
    {
        name: "John",
        imageURL:
        "https://media.licdn.com/dms/image/D4D03AQEMD_A0ml2-8Q/profile-displayphoto-shrink_200_200/0/1690322671325?e=1695859200&v=beta&t=tlIPfRksaoRVK9_MHdwjgsdhsBw5vd_pQwjlNnOYDtQ",
        linkedIn: "https://www.linkedin.com/in/colemanja/",
        gitlab: "https://gitlab.com/JohnC1992",
        bio: "Software developer and chess enthusiast. I love breaking down complex problems and sharing my knowledge with others.",
    },
];
