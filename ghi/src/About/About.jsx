import PersonCard from "./PersonCard";
import { AboutInfo } from "./Aboutinfo";

export default function About() {
  return (
    <div>
      <div className="space"></div>
      <h1 style={{ textIndent: 3 + "vw" }}>About Us</h1>
      <div className="space" />
      <div className="about-grid">
        {AboutInfo.map((person, i) => (
          <div key={person.name}>
            <PersonCard
              name={person.name}
              gitlab={person.gitlab}
              linkedin={person.linkedIn}
              pictureUrl={person.imageURL}
              bio={person.bio}
              side={i % 2 === 1 ? "right" : "left"}
            />
            <div className="divider" />
            <div className="space" />
          </div>
        ))}
      </div>
    </div>
  );
}
