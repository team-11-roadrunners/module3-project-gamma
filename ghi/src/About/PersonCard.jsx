import { Link } from "react-router-dom";
import LinkedinIcon from "./icons/LinkedinIcon";
import GitlabIcon from "./icons/GitlabIcon";

export default function PersonCard({
  props,
  name,
  gitlab,
  linkedin,
  pictureUrl,
  side,
  bio,
}) {
  return (
    <div {...props} className="container-fluid person-card">
      <div className={`profile-picture-name ${side}`}>
        <img
          className={`profile-pic preview-pic`}
          src={pictureUrl}
          alt={name}
        />
        <h3 className="">{name}</h3>
      </div>
      <div className="bio">
        <p className="text-center">{bio}</p>
        <GitlabIcon className="gitlab" link={gitlab} />
        <span>|</span>
        <Link to={linkedin}>
          <LinkedinIcon className="linkedin" />
        </Link>
      </div>
    </div>
  );
}
