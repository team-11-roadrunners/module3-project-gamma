import { AboutInfo } from "./Aboutinfo";
import PersonPreviewCard from "./PersonPreviewCard";

export default function AboutPreview({ props }) {
  return (
    <div {...props}>
      <h1 className="center-text">Meet the team</h1>
      <div className="about-preview">
        {AboutInfo.map((p) => (
          <PersonPreviewCard key={p.name} imageURL={p.imageURL} name={p.name} />
        ))}
      </div>
      <div className="divider"></div>
      <div className="space"></div>
    </div>
  );
}
