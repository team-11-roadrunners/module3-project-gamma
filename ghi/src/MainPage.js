import React, { useEffect, useState, useRef } from "react";
import { useNavigate } from "react-router-dom";
import AboutPreview from "./About/AboutPreview";
import GenreSlider from "./Genres/GenreSlider";

export default function MainPage() {
  const [picture, setPicture] = useState();
  const pictureRef = useRef(null);
  const [imageHeight, setImageHeight] = useState(0);

  const navigate = useNavigate();

  function updatePictureHeight() {
    if (pictureRef.current) {
      setImageHeight(pictureRef.current.clientHeight);
    }
  }

  useEffect(() => {
    const image_list = [
      "https://wallpaperaccess.com/full/36626.jpg",
      "https://wallpapers.com/images/featured/fullmetal-alchemist-brotherhood-0tvvnvos36llm4yx.webp",
      "https://i.pinimg.com/originals/62/4b/88/624b883a70c7560dff53bfe108457fea.jpg",
      "https://images6.alphacoders.com/112/1128080.jpg",
      "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a44778ea-3457-40e0-8979-b7e3685d23d0/dftr3y6-afc7b5f3-7b6e-4259-9a12-0867ac9fbc6b.png/v1/fill/w_1233,h_648,q_70,strp/jujutsu_kaisen_wallpaper_8k_loading_screen_by_newjer53_dftr3y6-pre.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTAxMCIsInBhdGgiOiJcL2ZcL2E0NDc3OGVhLTM0NTctNDBlMC04OTc5LWI3ZTM2ODVkMjNkMFwvZGZ0cjN5Ni1hZmM3YjVmMy03YjZlLTQyNTktOWExMi0wODY3YWM5ZmJjNmIucG5nIiwid2lkdGgiOiI8PTE5MjAifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.fVE2bSBHV-CE70zTrPf_gUiTM21HScC3ELzhnGbGf-I",
      "https://i.redd.it/hykpvdgukwt71.png",
      "https://img.asmedia.epimg.net/resizer/XWLzt1KnzGkj4ncJeT-QpCcZHOA=/1472x828/cloudfront-eu-central-1.images.arcpublishing.com/diarioas/5VPPR6ZMBFPJLCZHFMADMKEV7U.jpg",
      "https://www.looper.com/img/gallery/the-reason-jojos-bizarre-adventures-anime-style-changed-with-part-5/l-intro-1616603979.jpg",
      "https://dw9to29mmj727.cloudfront.net/promo/2016/6230-SeriesHeaders_OPM_2000x800.jpg",
      "https://wallpapers.com/images/featured/naruto-characters-peo1d7c1aqugno3e.jpg",
      "https://media.wired.co.uk/photos/606d9e779a15f73a597a1a15/4:3/w_2568,h_1926,c_limit/shutterstock_editorial_5874266e_huge.jpg",
      "https://i0.wp.com/cradleview.net/wp-content/uploads/2020/06/Z-6.png",
      "https://cdn.vox-cdn.com/thumbor/BU1QuBVA_EINIUdPxdTMg5GJGVo=/1400x0/filters:no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/24528018/bluelock_isagi_team.jpeg",
      "https://images.amcnetworks.com/ifccenter.com/wp-content/uploads/2019/12/nausicaa_01_nvw1012945.jpg",
      "https://i0.wp.com/film-bunker.com/wp-content/uploads/2019/11/dr-stone-kv-village-header.jpg",
      "https://wallpapers.com/images/hd/gundam-4k-ydnk09a74q6pu46l.jpg",
    ];
    setPicture(image_list[Math.floor(Math.random() * image_list.length)]);
  }, [setPicture]);

  return (
    <div>
      <div className="main-page-container">
        <img
          className="img-fluid front-page-image"
          onLoad={updatePictureHeight}
          ref={pictureRef}
          src={picture}
          alt={picture}
        />
        <div className="picture-text">
          <div>What anime</div>
          <div>should I watch?</div>
        </div>
        <div>
          <button
            className="randomize-btn"
            onClick={() => {
              navigate("/random");
            }}
          >
            RANDOMIZE!
          </button>
        </div>
      </div>
      <div style={{ height: imageHeight + "px" }}></div>
      <div className="space"/>
      <div>
        <GenreSlider />
      </div>
      <div className="space"/>
      <AboutPreview />
    </div>
  );
}
