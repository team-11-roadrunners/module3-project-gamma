export default function PrivacyPolicy() {
  return (
    <div className="privacy-policy">
      <h1>Privacy Policy for Randime Website</h1>
      <p>Effective Date: 08/01/2023</p>

      <p>
        At Randime, we are committed to protecting your privacy. This Privacy
        Policy outlines how we collect, use, disclose, and safeguard your
        personal information when you visit our website or use our services. By
        accessing or using our website, you consent to the practices described
        in this Privacy Policy.
      </p>

      <h2>1. Information We Collect:</h2>
      <p>
        <strong>1.1. Personal Information:</strong> We may collect personal
        information that you voluntarily provide to us when you create an
        account, subscribe to our newsletter, contact us through our contact
        forms, participate in surveys or promotions, or interact with our
        website in any other way. This information may include but is not
        limited to your name, email address, postal address, phone number, and
        any other information you choose to provide.
      </p>

      <p>
        <strong>1.2. Non-Personal Information:</strong> We may also collect
        non-personal information automatically when you visit our website. This
        information may include your IP address, browser type, operating system,
        device information, and the pages you visit on our website.
      </p>

      <h2>2. How We Use Your Information:</h2>
      <p>
        <strong>2.1. Personal Information:</strong> We may use your personal
        information to provide you with our services, respond to your inquiries
        and requests, send you promotional materials and updates about our
        website, process payments, and maintain your account.
      </p>

      <p>
        <strong>2.2. Non-Personal Information:</strong> Non-personal information
        is primarily used to analyze website usage trends, improve our website's
        content and functionality, and enhance the overall user experience.
      </p>

      <h2>3. Cookies and Similar Technologies:</h2>
      <p>
        <strong>3.1. Cookies:</strong> We use cookies and similar technologies
        (such as web beacons and pixels) to collect and store certain
        information about your interactions with our website. Cookies are small
        text files that are sent to your browser from our servers and stored on
        your device. They help us recognize you, remember your preferences, and
        analyze how our website is used.
      </p>

      <p>
        <strong>3.2. Third-Party Cookies:</strong> We may allow third-party
        service providers to place cookies on your device to assist us with
        various aspects of our website, such as analytics and advertising. These
        providers have their own privacy policies governing the use of
        information collected through cookies.
      </p>

      <h2>4. Data Sharing and Disclosure:</h2>
      <p>
        <strong>4.1. Service Providers:</strong> We may share your personal
        information with trusted third-party service providers who assist us in
        operating our website, conducting business, and providing services to
        you. These service providers are obligated to keep your information
        secure and are not allowed to use it for any other purpose.
      </p>

      <p>
        <strong>4.2. Legal Compliance:</strong> We may disclose your information
        if required to do so by law or in response to valid requests by public
        authorities (e.g., law enforcement).
      </p>

      <p>
        <strong>4.3. Business Transfers:</strong> In the event of a merger,
        acquisition, or sale of all or a portion of our assets, your information
        may be transferred as part of the transaction. We will notify you via
        email or a prominent notice on our website of any such change in
        ownership or control.
      </p>

      <h2>5. Your Choices and Rights:</h2>
      <p>
        <strong>5.1. Account Settings:</strong> You can review and update your
        account information by logging into your Randime account.
      </p>

      <p>
        <strong>5.2. Opt-Out:</strong> You have the right to opt-out of
        receiving promotional communications from us. You can do this by
        following the unsubscribe instructions included in our emails or by
        contacting us directly.
      </p>

      <p>
        <strong>5.3. Cookies:</strong> You can manage your cookie preferences
        through your browser settings. Please note that disabling certain
        cookies may affect the functionality and user experience of our website.
      </p>

      <h2>6. Security:</h2>
      <p>
        We implement appropriate technical and organizational measures to
        protect your personal information from unauthorized access, alteration,
        disclosure, or destruction.
      </p>

      <h2>7. Changes to this Privacy Policy:</h2>
      <p>
        We may update this Privacy Policy from time to time. Any changes will be
        effective when posted on this page, with the updated date stated at the
        top.
      </p>

      <h2>8. Contact Us:</h2>
      <p>
        If you have any questions or concerns about this Privacy Policy or our
        data practices, please contact us at contact@randime.com.
      </p>

      <p>
        By using Randime, you acknowledge that you have read and understood this
        Privacy Policy and agree to its terms.
      </p>
    </div>
  );
}
